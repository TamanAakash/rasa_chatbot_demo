# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
import random

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, UserUtteranceReverted

from langdetect import detect

class Greeting(Action):
    def name(self):
        return "action_greeting"
        
    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ["Hi there. 👋😃 It's such a pleasure to have you here. How can we help you?",
                        "Hello, 🤗 how can we assist you?"]
        else:
            messages = ["नमस्कार! 🙏 हामी तपाईलाई कसरी सहयोग गर्न सक्छौ?",
                        "नमस्ते! 🙏 तपाईलाई कसरी सहयोग गर्न सक्छौहोला हामीले?"]
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []

class AfterGreeting(Action):
    def name(self):
        return "action_after_greet"
    
    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ["I am fine. 😁 How can we help you.",
                        "I am doing very well. How can we assist you. 😁"]
        else:
            messages = ["म ठिक छु। 😃 तपाईलाई कसरी सहयोग गर्न सक्छौ?",
                        "म धेरै राम्रो गर्दै छु। 😃 हामी तपाईंलाई कसरी मद्दत गर्न सक्छौं?"]
        
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []

class Goodbye(Action):
    def name(self):
        return "action_goodbye"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ['Thank you, 😁 I am happy to help you. 👋',
                        'I hope I was helpful for you. 👋😁']
        else:
            messages = ["धन्यबाद, 🙏 म तपाईंलाई सहयोग गर्न पाएकोमा खुसी छु। 😁",
                        "धन्यबाद, 😁 आशा गर्दछु कि म तपाईको लागि सहयोगी थियो। 🙏"]

        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []


class OutOfScope(Action):
    def name(self):
        return "action_out_of_scope"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ['Sorry 😕 I didn’t catch that word. Could you tell me more clearly?',
                        'I am sorry I did not understand. 😕 Can you tell again? 🧐']
        else:
            messages = ["😕 माफ गर्नुहोस्, मैले त्यो शब्द बुजिन, के तपाईं मलाई अझै स्पष्ट बताउन सक्नुहुन्छ?",
                        "तपाईंले भन्नभाको मैले बुझिन। तपाईं फेरि भन्न सक्नुहुन्छ? 🧐"]

        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return [UserUtteranceReverted()]


class AfterOutOfScope(Action):
    def name(self):
        return "action_after_out_of_scope"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ['We are sorry for not being able to understand you. Could you provide us your contact details so that we could help you properly?',
                        "Sorry, I don't think I can help you. Can you give us your contant detail so that we could help you properly?"]
        else:
            messages = ["😕 माफ गर्नुहोस्, मैले त्यो शब्द बुजिन, के तपाईं मलाई अझै स्पष्ट बताउन सक्नुहुन्छ?",
                        "तपाईंले भन्नभाको मैले बुझिन। तपाईं फेरि भन्न सक्नुहुन्छ? 🧐"]

        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []

class About(Action):
    def name(self):
        return "action_about_chatbot"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ['I am your virtual assistant from Home Credit to help you through your questions concerning our services.👋😃',
                        '😁 I am here to guide you through your queries regarding Home Credit services.']
        else:
            messages = ["म तपाइँको प्रश्नहरुको मद्दत गर्ने तपाइँको सहायक हुँ। 👋😃",
                        "म यहाँ तपाइँको जिज्ञासाहरु बारे मद्दत गर्ने साथी हुँ। 😁"]
       
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []

class DocumentAcceptProof(Action):
    def name(self):
        return "action_document_accept_proof"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        try:
            proof_type = tracker.get_slot('proof_type')
        except:
            messages = ["Sorry, I cannot understand you. Could you repeat it again?", "I am having confusion in understanding it. Would you repeat it please?",
				"I find it quite ambiguous. Can you tell me again a bit clearly?"]
            reply = random.choice(messages)
            attachment = {
                "query_response": reply,
                "data":[],
                "type":"normal_message",
                "data_fetch_status": "success"
            }
            dispatcher.utter_message(text=reply)
            return [UserUtteranceReverted()]
        # print (proof_type)
        q = tracker.latest_message
        # print (q)
        if detect(question) != "hi":
            if proof_type is None:
                messages = ["Documents are required for verifying these two things: -\n1. Personal identity proof \n2. Income and address proof",
				    "The documents required for verifying differs in these two things: -\n1. Personal identity proof \n2. Income and address proof"]
                buttons = [
                    {"title":"Personal Identity proof","payload":"document personal identity proof"},
                    {"title":"Income and Address proof","payload":"document income and address proof"}
                ]
                reply = random.choice(messages)
                attachment = {
                    "query_response": reply,
                    "data":[],
                    "type":"normal_message",
                    "data_fetch_status": "success"
                }
                dispatcher.utter_message(text=reply,buttons=buttons)
                return []
            if proof_type == "identity proof" or proof_type == "id":
                reply = "The following documents are accepted as valid Identity Proof: \n* PAN Card \n* If PAN Card is not available, then Form 60 with one of the three below documents \n* Passport \n* Voter ID Card \n* Driving License"
            else:
                reply = "The following documents are accepted as valid Income and Address Proof: \n* Voter ID Card \n* Driving License \n* Passport \n* Bank Passbook / Bank Statement \n* Government House Allotment Letter \n* Property Tax Recepit \nNote: Conditions apply. Home Credit India reserves the right to call upon additional documents at its discretion."
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []


class buy_phones_on_emi(Action):
    def name(self):
        return "action_buy_phones_on_emi"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            reply = "We offer Mobile Phone on EMIs up to '100%' of the phone value on zero or very low-interest rates. To avail the Mobile Phone on EMIs, head to the nearest Home Credit partner shop. For Home Credit partner shops near you <a href='https://www.homecredit.co.in/en/shop-locator'>Click Here</a>"
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []


class service_types(Action):
    def name(self):
        return "action_services"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ["Home Credit provide following services: ","The services that we provide in Home Credit are: ",
                        "Currently we have been providing our customer the given services: "]
            buttons = [
                {"title":"Loan","payload":"types of loan"},
                {"title":"Insurance","payload":"types of insurance"},
            ]
        else:
            reply = []
       
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"message_with_buttons",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply,buttons=buttons)
        return []

class type_of_loan(Action):
    def name(self):
        return "action_type_of_loan"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            reply = "We provide attractive loan on the following areas:"
            buttons = [
                {"title":"Mobile Phone on EMIs","payload":"about mobile phone"},
                {"title":"Home Appliances on EMIs","payload":"about home appliance"},
                {"title":"Personal Loan","payload":"about personal loan"},
                {"title":"Home Credit Ujjwal Card","payload":"about ujjwal card"},
                {"title":"Two Wheeler Loan","payload":"about two wheeler"},
                {"title":"Home Loan","payload":"about home loan"}
            ]
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"message_with_buttons",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply,buttons=buttons)
        return []

class type_of_insurance(Action):
    def name(self):
        return "action_type_of_insurance"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            reply = "We provide attractive and best insurance on the following areas:"
            buttons = [
                {"title":"Health Insurance","payload":"about health insurance"},
                {"title":"Pocket Insurance","payload":"about pocket insurance"}
            ]
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"message_with_buttons",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply,buttons=buttons)
        return []

class about_loan_service(Action):
    def name(self):
        return "action_about_loan_service"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        # specific_service_type = tracker.latest_message['entities'][0]
        try:
            specific_service_type = tracker.get_slot('loan_type')
        except:
            messages = ["Sorry, I cannot understand you. Could you repeat it again?", "I am having confusion in understanding it. Would you repeat it please?",
				"I find it quite ambiguous. Can you tell me again a bit clearly?"]
            reply = random.choice(messages)
            attachment = {
                "query_response": reply,
                "data":[],
                "type":"normal_message",
                "data_fetch_status": "success"
            }
            dispatcher.utter_message(text=reply)
            return [UserUtteranceReverted()]
        if detect(question) != "hi":
            if specific_service_type == "ujjwal card":
                reply = "Ujjwal Card is an instant limit to purchase smartphones or home appliances on easy monthly EMIs. You can learn more about it by clicking <a href='https://www.homecredit.co.in/en/ujjwal-card'>Here</a>"
            elif specific_service_type == "personal loan":
                reply = "Home Credit provides you an attractive personal loan. To learn more in details click <a href='https://www.homecredit.co.in/en/personal-loan'>Here</a>"
            elif specific_service_type == "home appliance":
                reply = "Home appliances on EMIs is a service provided by Home Credit to our customers for buying household items. To learn more click <a href='https://www.homecredit.co.in/en/home-appliances-on-emis'>Here</a>"
            elif specific_service_type == "mobile phone":
                reply = "You can now by Mobile phones on EMIs through Home Credit. To know more about it clikc <a href='https://www.homecredit.co.in/en/mobiles-on-emi'>Here</a>"
            elif specific_service_type == "two wheeler":
                reply = "Home Credit provides you with attractive loan for buying two wheeler items like bikes and scooters. Learn more about this service from <a href='https://www.homecredit.co.in/en/two-wheeler-loan'>Here</a>"
            elif specific_service_type == "home loan":
                reply = "Getting home loan has been made very easy by Home Credit. To know more click <a href='https://www.homecredit.co.in/online-services/productdetails/IndiaShelter?utm_source=CorpWebsite&utm_medium=HomeCredit&utm_campaign=direct&utm_term=IndiaShelter'>Here</a>"
            else:
                reply = "Sorry, I did not get about the type of loan you are saying. Can you repeat a bit more clearly?"

        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return [SlotSet('loan_type', None)]

class about_insurance_service(Action):
    def name(self):
        return "action_about_insurance_service"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        # specific_service_type = tracker.latest_message['entities'][0]
        try:
            specific_service_type = tracker.get_slot('insurance_type')
        except:
            messages = ["Sorry, I cannot understand you. Could you repeat it again?", "I am having confusion in understanding it. Would you repeat it please?",
				"I find it quite ambiguous. Can you tell me again a bit clearly?"]
            reply = random.choice(messages)
            attachment = {
                "query_response": reply,
                "data":[],
                "type":"normal_message",
                "data_fetch_status": "success"
            }
            dispatcher.utter_message(text=reply)
            return [UserUtteranceReverted()]
        if detect(question) != "hi":
            if specific_service_type == "health insurance":
                reply = "Keeping your health in proiority, Home Credit provides you with attractive health insurance. To know more <a href = 'https://www.homecredit.co.in/online-services/productdetails/HealthInsurance?utm_source=CorpWebsite&utm_medium=HomeCredit&utm_campaign=direct'>Click Here</a>"
            elif specific_service_type == "pocket insurance":
                reply = "Home Credit provides you with attractive pocket insurance to cover for your everyday risks. To know more about it <a href ='https://www.homecredit.co.in/en/pocket-insurance'>Click Here</a>"
            else:
                reply = "Sorry, I did not get about the type of insurance you are saying. Can you repeat a bit more clearly?"
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return [SlotSet('insurance_type', None)]

class about_product_service(Action):
    def name(self):
        return "action_about_product_service"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        # specific_service_type = tracker.latest_message['entities'][0]
        try:
            specific_service_type = tracker.get_slot('type_of_product')
        except:
            messages = ["Sorry, I cannot understand you. Could you repeat it again?", "I am having confusion in understanding it. Would you repeat it please?",
				"I find it quite ambiguous. Can you tell me again a bit clearly?"]
            reply = random.choice(messages)
            attachment = {
                "query_response": reply,
                "data":[],
                "type":"normal_message",
                "data_fetch_status": "success"
            }
            dispatcher.utter_message(text=reply)
            return [UserUtteranceReverted()]
        if detect(question) != "hi":
            if specific_service_type == "safe pay":
                reply = "Safe Pay is a value-added service offered to Home Credit consumer durable and personal loan customers. It is a package that offers Payment Holiday,  Free Early Repayment and Life Cover."
            elif specific_service_type == "screen guard":
                reply = "The ScreenGuard plan is an exclusive phone screen protection plan to cover the cost of repairing the damaged mobile phone screen."
            else:
                reply = "Sorry, I did not get about the type of product you are saying. Can you repeat a bit more clearly?"

        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return [SlotSet('type_of_product', None)]


class maximum_available_loan(Action):
    def name(self):
        return "action_maximum_available_loan"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        # loan_type = tracker.latest_message['entities'][0]['entity']
        try:
            loan_type = tracker.get_slot('loan_type')
        except:
            messages = ["Sorry, I cannot understand you. Could you repeat it again?", "I am having confusion in understanding it. Would you repeat it please?",
				"I find it quite ambiguous. Can you tell me again a bit clearly?"]
            reply = random.choice(messages)
            attachment = {
                "query_response": reply,
                "data":[],
                "type":"normal_message",
                "data_fetch_status": "success"
            }
            dispatcher.utter_message(text=reply)
            return [UserUtteranceReverted()]
        if detect(question) != "hi":
            if loan_type == "mobile phone":
                reply = "Home Credit Mobile Phone on EMIs can be availed maximum up to ₹60,000."
            elif loan_type == "home appliance":
                reply = "Home Credit finances home appliances up to the maximum value of ₹1,00,000."
            elif loan_type == "personal loan":
                reply = "Home Credit offers personal loans up to the maximum value of  ₹2,40,000."
            else:
                reply = "Sorry, right now we can only disclose the amount of the following type of loan: - \n1. Mobile Phone on EMIs \n2. Home Appliances on EMIs \n3. Personal Loan"
        else:
            messages = []
       
        # reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return [SlotSet('loan_type', None)]

class card_required_for_emi(Action):
    def name(self):
        return "action_card_required_for_emi"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ["No, you do not need either Credit or Debit Card from our Home Credit partner shops.",
                        "To avail EMI from Home Credit partner shops, you do not need Credit or Debit Card."]
        else:
            messages = []
       
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []

class gap_between_loan(Action):
    def name(self):
        return "action_gap_between_loan"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        question = tracker.latest_message['text']
        if detect(question) != "hi":
            messages = ["There should be a minimum of 90 days’ gap between two Home Credit loan applications.",
                        "In order to get multiple loans from Home Credit, there has to be a gap of atleast 90 days between two loan applications."]
        else:
            messages = []
       
        reply = random.choice(messages)
        attachment = {
			"query_response": reply,
			"data":[],
			"type":"normal_message",
			"data_fetch_status": "success"
		}
        dispatcher.utter_message(text=reply)
        return []